package com.example.dance.repository;

import com.example.dance.domain.Group;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {

    @EntityGraph(attributePaths = "members")
    List<Group> findAll();
    @EntityGraph(attributePaths = "members")
    Optional<Group> findById(Long id);
    boolean existsBySchedule_Id(Long scheduleId);
    boolean existsByLesson_Id(Long lessonId);
}

package com.example.dance.controller;

import com.example.dance.Main;
import com.example.dance.domain.Group;
import com.example.dance.domain.Member;
import com.example.dance.repository.GroupRepository;
import com.example.dance.repository.MemberRepository;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Controller
public class GroupMemberController {
    private final MemberRepository memberRepository;
    private final GroupRepository groupRepository;

    @Autowired
    public GroupMemberController(MemberRepository memberRepository, GroupRepository groupRepository) {
        System.out.println(member);
        System.out.println(group);
        this.memberRepository = memberRepository;
        this.groupRepository = groupRepository;
    }

    @FXML
    private TextField member;

    @FXML
    private TextField group;

    public void addMember() {
        Group group = groupRepository.findById(Long.valueOf(this.group.getText())).get();
        Member member = memberRepository.findById(Long.valueOf(this.member.getText())).get();

        group.addMember(member);

        groupRepository.save(group);

        PopupWindow.openWindow("Добавлен", "Участник добавлен в группу");
    }

    @FXML
    void add(ActionEvent event) {
        if (!validate()) {
            return;
        }

        addMember();
    }

    public void deleteMember() {
        Group group = groupRepository.findById(Long.valueOf(this.group.getText())).get();
        Member member = memberRepository.findById(Long.valueOf(this.member.getText())).get();

        group.deleteMember(member.getId());

        groupRepository.save(group);

        PopupWindow.openWindow("Удалён", "Участник удалён из группы");
    }

    @FXML
    void delete(ActionEvent event) {
        System.out.println(member);
        System.out.println(group);
        if (!validate()) {
            return;
        }

        deleteMember();
    }

    private boolean validate() {
        System.out.println(member);
        System.out.println(group);
        if (!member.getText().matches("[0-9]+")) {
            PopupWindow.openWindow("Ошибка", "ID участника должно быть целочисленным");
            return false;
        }

        if (!group.getText().matches("[0-9]+")) {
            PopupWindow.openWindow("Ошибка", "ID группы должно быть целочисленным");
            return false;
        }

        Long groupId = Long.valueOf(group.getText());
        Long memberId = Long.valueOf(member.getText());

        if (!memberRepository.existsById(memberId)) {
            PopupWindow.openWindow("Ошибка", "Участника с таким ID не существует");
            return false;
        }

        if (!groupRepository.existsById(groupId)) {
            PopupWindow.openWindow("Ошибка", "Группы с таким ID не существует");
            return false;
        }

        return true;
    }

    public void groups(ActionEvent actionEvent) throws IOException {
        GroupController.load();
    }

    public static void load() throws IOException {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/group_member.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }
}

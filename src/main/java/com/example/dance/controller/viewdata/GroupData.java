package com.example.dance.controller.viewdata;

import lombok.Data;

@Data
public class GroupData {
    private Long id;
    private Long scheduleId;
    private Long lessonId;
    private String memberIds;
}

package com.example.dance.controller;

import com.example.dance.Main;
import com.example.dance.domain.Lesson;
import com.example.dance.repository.LessonRepository;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;

@Controller
public class LessonController {
    private final LessonRepository lessonRepository;

    @FXML
    private TextField danceName;

    @FXML
    private TextField danceType;

    @FXML
    private TextField duration;

    @FXML
    private TextField price;

    @FXML
    private TableView<Lesson> table;

    public LessonController(LessonRepository lessonRepository) {
        this.lessonRepository = lessonRepository;
    }

    @Transactional
    @FXML
    void add(ActionEvent event) {
        if (danceName.getText().length() == 0) {
            PopupWindow.openWindow("Ошибка", "Заполните название");
            return;
        }

        if (danceType.getText().length() == 0) {
            PopupWindow.openWindow("Ошибка", "Заполните тип");
            return;
        }

        if (!duration.getText().matches("[0-9]+")) {
            PopupWindow.openWindow("Ошибка", "Продолжительность должна быть целочисленной");
            return;
        }

        if (!price.getText().matches("[0-9]+[.]?[0-9]*")) {
            PopupWindow.openWindow("Ошибка", "Цена может содержать только цифры и точку(если требуется)");
            return;
        }

        Lesson lesson = new Lesson();
        lesson.setDanceName(danceName.getText());
        lesson.setDanceType(danceType.getText());
        lesson.setDurationInMinutes(Integer.valueOf(duration.getText()));
        lesson.setPrice(new BigDecimal(price.getText()));

        lessonRepository.save(lesson);

        updateTable();
    }

    @Transactional
    void updateTable() {
        TableColumn<Lesson, Lesson> idCol = new TableColumn<>("ID");
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Lesson, Lesson> nameCol = new TableColumn<>("Название танца");
        nameCol.setCellValueFactory(new PropertyValueFactory<>("danceName"));

        TableColumn<Lesson, Lesson> typeCol = new TableColumn<>("Тип танца");
        typeCol.setCellValueFactory(new PropertyValueFactory<>("danceType"));

        TableColumn<Lesson, Lesson> durationCol = new TableColumn<>("Продолжительность");
        durationCol.setCellValueFactory(new PropertyValueFactory<>("durationInMinutes"));

        TableColumn<Lesson, Lesson> priceCol = new TableColumn<>("Стоимость");
        priceCol.setCellValueFactory(new PropertyValueFactory<>("price"));

        TableColumn<Lesson, Lesson> delete = new TableColumn<>("");
        delete.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        delete.setCellFactory(param -> new TableCell<Lesson, Lesson>() {
            private final Button deleteButton = new Button("Удалить");

            @Override
            protected void updateItem(Lesson lesson, boolean empty) {
                super.updateItem(lesson, empty);

                if (lesson == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(deleteButton);
                deleteButton.setOnAction(event -> {
                    try {
                        lessonRepository.deleteById(lesson.getId());
                        updateTable();
                    } catch (Exception e) {
                        PopupWindow.openWindow("Ошибка", "Запись не найдена");
                    }
                });
            }
        });

        ObservableList<Lesson> values = FXCollections
                .observableArrayList(lessonRepository.findAll());

        table.setItems(values);
        table.getColumns().clear();

        table.getColumns().addAll(
                idCol, nameCol,
                typeCol, durationCol,
                priceCol, delete
        );
    }
    @FXML
    public void initialize() {
        updateTable();
    }
    public static void load() throws IOException {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/lesson.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }

    public void main(ActionEvent actionEvent) throws IOException {
        MainController.load();
    }
}

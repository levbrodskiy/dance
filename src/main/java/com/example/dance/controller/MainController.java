package com.example.dance.controller;

import com.example.dance.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import org.springframework.stereotype.Controller;

import java.io.IOException;

@Controller
public class MainController {

    @FXML
    void group(ActionEvent event) throws IOException {
        GroupController.load();
    }

    @FXML
    void lesson(ActionEvent event) throws IOException {
        LessonController.load();
    }

    @FXML
    void member(ActionEvent event) throws IOException {
        MemberController.load();
    }

    @FXML
    void schedule(ActionEvent event) throws IOException {
        ScheduleController.load();
    }

    public static void load() throws IOException {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/main.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }
}

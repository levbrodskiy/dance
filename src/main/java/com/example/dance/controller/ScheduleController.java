package com.example.dance.controller;

import com.example.dance.Main;
import com.example.dance.domain.Schedule;
import com.example.dance.repository.ScheduleRepository;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Controller
public class ScheduleController {
    private final ScheduleRepository scheduleRepository;

    @Autowired
    public ScheduleController(ScheduleRepository scheduleRepository) {
        this.scheduleRepository = scheduleRepository;
    }

    @FXML
    private TextField day;

    @FXML
    private TextField time;

    @FXML
    private TableView<Schedule> table;

    @Transactional
    @FXML
    void add(ActionEvent event) {
        if (day.getText().length() == 0) {
            PopupWindow.openWindow("Ошибка", "Заполните имя");
            return;
        }
        System.out.println(time.getText());
        if (!time.getText().matches("[0-9]{2}:[0-9]{2}")) {
            PopupWindow.openWindow("Ошибка", "Введите корректное время (18:35)");
            return;
        }
        System.out.println(time.getText());

        Integer hours = Integer.valueOf(time.getText().split(":")[0]);
        Integer minutes = Integer.valueOf(time.getText().split(":")[1]);

        if (hours > 23 || minutes > 59) {
            PopupWindow.openWindow("Ошибка", "Введите корректное время (18:35)");
            return;
        }

        Schedule schedule = new Schedule();
        schedule.setId(null);
        schedule.setDayOfWeak(day.getText());
        schedule.setTime(time.getText());

        scheduleRepository.save(schedule);
        updateTable();
    }

    @Transactional
    void updateTable() {
        TableColumn<Schedule, Schedule> idCol = new TableColumn<>("ID");
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Schedule, Schedule> dayCol = new TableColumn<>("День недели");
        dayCol.setCellValueFactory(new PropertyValueFactory<>("dayOfWeak"));

        TableColumn<Schedule, Schedule> timeCol = new TableColumn<>("Время");
        timeCol.setCellValueFactory(new PropertyValueFactory<>("time"));

        TableColumn<Schedule, Schedule> delete = new TableColumn<>("");
        delete.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        delete.setCellFactory(param -> new TableCell<Schedule, Schedule>() {
            private final Button deleteButton = new Button("Удалить");

            @Override
            protected void updateItem(Schedule schedule, boolean empty) {
                super.updateItem(schedule, empty);

                if (schedule == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(deleteButton);
                deleteButton.setOnAction(event -> {
                    try {
                        scheduleRepository.deleteById(schedule.getId());
                        updateTable();
                    } catch (Exception e) {
                        PopupWindow.openWindow("Ошибка", "Запись не найдена");
                    }
                });
            }
        });

        ObservableList<Schedule> values = FXCollections
                .observableArrayList(scheduleRepository.findAll());

        table.setItems(values);
        table.getColumns().clear();

        table.getColumns().addAll(
                idCol, dayCol,
                timeCol, delete
        );
    }
    @FXML
    public void initialize() {
        updateTable();
    }
    public static void load() throws IOException {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/schedule.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }

    public void main(ActionEvent actionEvent) throws IOException {
        MainController.load();
    }
}

package com.example.dance.controller;

import com.example.dance.Main;
import com.example.dance.controller.viewdata.GroupData;
import com.example.dance.domain.Group;
import com.example.dance.domain.Lesson;
import com.example.dance.domain.Member;
import com.example.dance.domain.Schedule;
import com.example.dance.repository.GroupRepository;
import com.example.dance.repository.LessonRepository;
import com.example.dance.repository.MemberRepository;
import com.example.dance.repository.ScheduleRepository;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class GroupController {
    private final GroupRepository groupRepository;
    private final MemberRepository memberRepository;
    private final LessonRepository lessonRepository;
    private final ScheduleRepository scheduleRepository;

    @Autowired
    public GroupController(GroupRepository groupRepository, MemberRepository memberRepository, LessonRepository lessonRepository, ScheduleRepository scheduleRepository) {
        this.groupRepository = groupRepository;
        this.memberRepository = memberRepository;
        this.lessonRepository = lessonRepository;
        this.scheduleRepository = scheduleRepository;
    }

    @FXML
    private TextField lesson;

    @FXML
    private TextField schedule;

    @FXML
    private TextField member;

    @FXML
    private TableView<GroupData> table;

    @Transactional
    @FXML
    void add(ActionEvent event) {
        if (!this.lesson.getText().matches("[0-9]+")) {
            PopupWindow.openWindow("Ошибка", "ID занятия должно быть целочисленным");
            return;
        }

        if (!this.schedule.getText().matches("[0-9]+")) {
            PopupWindow.openWindow("Ошибка", "ID расписания должно быть целочисленным");
            return;
        }

        if (!this.member.getText().matches("[0-9]+")) {
            PopupWindow.openWindow("Ошибка", "ID участника должно быть целочисленным");
            return;
        }

        Long lessonId = Long.valueOf(this.lesson.getText());

        Optional<Lesson> lesson = lessonRepository.findById(lessonId);

        if (!lesson.isPresent()) {
            PopupWindow.openWindow("Ошибка", "Занятия с таким ID не существует");
            return;
        }

        Long scheduleId = Long.valueOf(this.schedule.getText());

        Optional<Schedule> schedule = scheduleRepository.findById(scheduleId);

        if (!schedule.isPresent()) {
            PopupWindow.openWindow("Ошибка", "Расписание с таким ID не существует");
            return;
        }

        Long memberId = Long.valueOf(this.member.getText());

        Optional<Member> member = memberRepository.findById(memberId);

        if (!member.isPresent()) {
            PopupWindow.openWindow("Ошибка", "Участника с таким ID не существует");
            return;
        }

        if (groupRepository.existsByLesson_Id(lesson.get().getId())) {
            PopupWindow.openWindow("Ошибка", "Занятие с этим ID занято");
            return;
        }

        if (groupRepository.existsBySchedule_Id(schedule.get().getId())) {
            PopupWindow.openWindow("Ошибка", "Расписание с этим ID занято");
            return;
        }

        Group group = new Group();
        group.setSchedule(schedule.get());
        group.setLesson(lesson.get());
        group.addMember(member.get());

        groupRepository.save(group);
        updateTable();
    }
    @FXML
    public void initialize() {
        updateTable();
    }
    @Transactional
    void updateTable() {
        TableColumn<GroupData, GroupData> idCol = new TableColumn<>("ID");
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<GroupData, GroupData> lessonCol = new TableColumn<>("ID занятия");
        lessonCol.setCellValueFactory(new PropertyValueFactory<>("lessonId"));

        TableColumn<GroupData, GroupData> scheduleCol = new TableColumn<>("ID расписания");
        scheduleCol.setCellValueFactory(new PropertyValueFactory<>("scheduleId"));

        TableColumn<GroupData, GroupData> membersCol = new TableColumn<>("ID участников");
        membersCol.setCellValueFactory(new PropertyValueFactory<>("memberIds"));

        TableColumn<GroupData, GroupData> delete = new TableColumn<>("");
        delete.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        delete.setCellFactory(param -> new TableCell<GroupData, GroupData>() {
            private final Button deleteButton = new Button("Удалить");

            @Transactional
            @Override
            protected void updateItem(GroupData data, boolean empty) {
                super.updateItem(data, empty);

                if (schedule == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(deleteButton);
                deleteButton.setOnAction(event -> {
                    try {
                        groupRepository.deleteById(data.getId());
                        updateTable();
                    } catch (Exception e) {
                        PopupWindow.openWindow("Ошибка", "Запись не найдена");
                    }
                });
            }
        });

        ObservableList<GroupData> values = FXCollections
                .observableArrayList(groupRepository.findAll().stream().map(g -> {
                    GroupData groupData = new GroupData();
                    groupData.setId(g.getId());
                    groupData.setLessonId(g.getLesson().getId());
                    groupData.setScheduleId(g.getSchedule().getId());

                    String members = "";

                    for (Member m : g.getMembers()) {
                        members += m.getId() + "/" + m.getName() + "\n";
                    }

                    groupData.setMemberIds(members);

                    return groupData;
                }).collect(Collectors.toList()));

        table.setItems(values);
        table.getColumns().clear();

        table.getColumns().addAll(
                idCol, lessonCol,
                scheduleCol, membersCol, delete
        );
    }

    public static void load() throws IOException {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/group.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }

    public void main(ActionEvent actionEvent) throws IOException {
        MainController.load();
    }

    public void members(ActionEvent actionEvent) throws IOException {
        GroupMemberController.load();
    }
}

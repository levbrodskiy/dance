package com.example.dance.controller;

import com.example.dance.Main;
import com.example.dance.domain.Member;
import com.example.dance.repository.MemberRepository;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Controller
public class MemberController {
    private final MemberRepository memberRepository;

    @FXML
    private TextField name;

    @FXML
    private TextField age;

    @FXML
    private TextField sex;

    @FXML
    private TableView<Member> table;

    @Autowired
    public MemberController(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }
    @FXML
    public void initialize() {
        updateTable();
    }
    @Transactional
    @FXML
    void add(ActionEvent event) {
        if (name.getText().length() == 0) {
            PopupWindow.openWindow("Ошибка", "Заполните имя");
            return;
        }

        if (sex.getText().length() == 0) {
            PopupWindow.openWindow("Ошибка", "Заполните пол");
            return;
        }

        if (!sex.getText().equals("муж") && !sex.getText().equals("жен")) {
            PopupWindow.openWindow("Ошибка", "Заполните пол (муж/жен)");
            return;
        }

        if (!age.getText().matches("[0-9]+")) {
            PopupWindow.openWindow("Ошибка", "Введите корректный возраст");
            return;
        }

        Member member = new Member();
        member.setId(null);
        member.setName(name.getText());
        member.setSex(sex.getText());
        member.setAge(Integer.valueOf(age.getText()));

        memberRepository.save(member);
        updateTable();
    }

    @Transactional
    void updateTable() {
        TableColumn<Member, Member> idCol = new TableColumn<>("ID");
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Member, Member> nameCol = new TableColumn<>("Имя");
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Member, Member> ageCol = new TableColumn<>("Возраст");
        ageCol.setCellValueFactory(new PropertyValueFactory<>("age"));

        TableColumn<Member, Member> sexCol = new TableColumn<>("Пол");
        sexCol.setCellValueFactory(new PropertyValueFactory<>("sex"));

        TableColumn<Member, Member> delete = new TableColumn<>("");
        delete.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        delete.setCellFactory(param -> new TableCell<Member, Member>() {
            private final Button deleteButton = new Button("Удалить");

            @Override
            protected void updateItem(Member member, boolean empty) {
                super.updateItem(member, empty);

                if (member == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(deleteButton);
                deleteButton.setOnAction(event -> {
                    try {
                        memberRepository.deleteById(member.getId());
                        updateTable();
                    } catch (Exception e) {
                        PopupWindow.openWindow("Ошибка", "Запись не найдена");
                    }
                });
            }
        });

        ObservableList<Member> values = FXCollections
                .observableArrayList(memberRepository.findAll());

        table.setItems(values);
        table.getColumns().clear();

        table.getColumns().addAll(
                idCol, nameCol,
                ageCol, sexCol,
                delete
        );
    }

    public static void load() throws IOException {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/member.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }

    public void main(ActionEvent actionEvent) throws IOException {
        MainController.load();
    }
}

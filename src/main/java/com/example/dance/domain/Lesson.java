package com.example.dance.domain;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "lessons")
public class Lesson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String danceName;
    private String danceType;
    private Integer durationInMinutes;
    private BigDecimal price;
}

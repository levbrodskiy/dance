package com.example.dance.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "groups")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany
    private List<Member> members = new ArrayList<>();
    @OneToOne
    private Lesson lesson;
    @OneToOne
    private Schedule schedule;

    public void addMember(Member member) {
        if (!members.stream().anyMatch(m -> m.getId().equals(member.getId()))) {
            members.add(member);
        }
    }

    public void deleteMember(long memberId) {
        members.removeIf( member -> member.getId().equals(memberId));
    }
}
